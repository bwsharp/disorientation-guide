---
title: "Manifest"
type: docs
date: 11 May 2021
---

# UW-Madison [dis]orientation guide

`Last updated May 2021. Always and forever a work-in-progress.`

## (Moving beyond a) Land Acknowledgment

The University of Wisconsin–Madison occupies ancestral Ho-Chunk land, a place their nation has called Teejop since time immemorial.[^1] In addition to Teejop, the University of Wisconsin continues to benefit from land stolen from the Chippewa and Menominee Nations which was laundered through the Morill Land Grant Act.[^2] As an institution of higher education, the University of Wisconsin is enmeshed in the same colonial networks of state power which perpetuated the mass dispossession of land from Indigenous nations and genocide against Indigenous peoples across Turtle Island.

In 1832, the Ho-Chunk (sometimes called Winnebago) Nation was coerced into signing a treaty which gave the United States legal pretext to ethnically cleanse their homelands.[^3] The Ho-Chunk Nation resisted decades of ethnic cleansing instituted by the United States Government and its armed forces and continually returned to their homelands—including by exploiting sections of the Homestead Act of 1862 to regain title to their ancestral lands.[^4]

While the University claims that “[t]his history of colonization informs our shared future of collaboration and innovation,”[^1] we encourage you to consider whether this history of colonial genocide informs those futures that UW does not consider in “collaboration” with the Ho-Chunk nation, the 12 First Nations which reside in what is now called ‘Wisconsin,’ or the Indigenous polities whose dispossession and genocide created the preconditions for UW to exist in the first place. The University’s official land acknowledgment is as notable for what it says as it is for what it leaves out: specifically, it leaves out what it will do with its newfound consciousness of its participation and benefits from these histories of genocide. It leaves out what it will do with its stolen land and the wealth derived from that theft. It leaves out what comes *after* the land acknowledgment. Today, UW-Madison respects the inherent sovereignty of the Ho-Chunk Nation, along with the eleven other First Nations of Wisconsin, but what about tomorrow?

Writer and educator Chelsea Vowel notes that “Moving beyond territorial acknowledgments means asking hard questions about what needs to be done once we’re ‘aware of Indigenous presence’. It requires that we remain uncomfortable, and it means making concrete, disruptive change.”[^5] Unfortunately, the offices in Bascom hall are all too comfortable. Their climate control insulates them from the rising heat, and it is your job to make them sweat in their worsted suits. 

## Theorizing [dis]orientation

Student orientation is the first *college experience™* for many of us. UW markets its orientation as *SOAR:* an experience for new students to get the ‘lay of the land’ as they explore the new ‘frontier’ of higher education in a way that evokes a God’s eye survey of the campus from above. SOAR creates the identity of the ‘Badger’ as always-already more than a student. Administrators declare that the program aims to “[e]xpose new students to the array of academic and social opportunities” and “integrat[e] new students into the life of the University”.[^6] We argue that by orienting students toward campus and the ‘Badger’ life, the university makes us unwitting participants in economies of extraction and death.

SOAR intensifies the process of creating the Badger identity in no small part by orienting students toward the space of campus in a particular way. During the quintessential campus tour, new Badgers are oriented toward the landmarks of campus: Camp Randall, Bascom Hill, Memorial Union, and Union South. This orientation conveniently excludes other aspects of campus space, including the colonial history of Teejop; the psychological effects of the whiteness of campus space upon students of color; the effects of campus on local cost of housing; and the ways in which campus boundaries give pretext for violence against houseless neighbors. At the same time, SOAR continues the bureaucratic processes which orient student lives toward the affective economy of credits and debts. UW administration works to control student life during SOAR by educating Badgers about degree requirements, opportunities for academic distinction, and opportunities for networking and community ‘service.’ In sum, SOAR is part of a larger network of processes which create the idea of a ‘Badger’ community. In doing so students become oriented toward some histories, objects, and landscapes at the expense of others. We will see later that UW, like all universities, is embedded in economies of extraction, harm, and death; by orienting us toward the institution of the university, the administration makes us unwittingly complicit in the production of structural violence.

Which spaces are made invisible when the Badger SOAR-s over campus? What if we don’t want to be oriented by the administration—what if we want to be oriented away from the ‘Badger’ identity? What if we want to be *dis-*oriented? Philosopher Sara Ahmed says that disorientation can be an ordinary feeling which comes and goes as we move throughout space and time.[^7] Ahmed thinks that those moments are breaks in our life-lines which can orient us toward something other. In so doing, moments of disorientation create the possibility for imagining different ways of being in the world—both on the scale of the personal and the scale of the political.[^8] For many of us, the next four years are an ongoing disorientation: a turning-away from the spaces and opportunities that the administration lays out for us in an effort to walk in different spaces and live different lives.

## “Education” and modes of studying otherwise

Everyone can tell you about their experience of burnout in education. Burnout is that point in the semester when you just can’t bring yourself to do the work you signed up for: a mutiny of your body-mind which is paradoxically accompanied by the creeping anxiety that you are not doing enough. Psychologically, we feel burnout when we have a lack of control over our work or environment, unclear expectations, dysfunctional social relations, extremes in activity (or lack of activity), or an erosion of the boundaries and balance between work and other aspects of life.[^9] Make no mistake about it: your higher education is, in fact, work—your work as a student (as well as your tuition dollars) is an essential input for the economies of knowledge-production, intellectual property, and human capital which characterize contemporary higher education. Burnout is the emotional warning sign that these economies have different values than we do. Put differently, burnout is firsthand evidence that the economies of higher education do not value our personal, psycho-emotional, or physical well-being.

It is a good thing, then, that there are alternatives to education. Scholar Eli Meyerhoff argues that education is nothing more than one possible mode of study, albeit one with hegemonic dominance in the United States.[^10] Simply put, a mode of study is just a set of relationships between (1) those who dedicate attention to the world and (2) the specific phenomena they are attending to. As Meyerhoff notes, “[t]his sustained attention modifies their capacities and dispositions for understanding the world.”[^11]  The education-based mode of study is one which structures those relations in a vertical hierarchy, with students moving up the levels of schooling; which uses techniques of discipline and governance to shape students’ identities; which sees expert knowledge as universally valid and reproducible; and which aligns academic success with moral virtue and academic failure with shame.[^12] Consequently, education is a highly structured mode of study which shapes the minds and identities of students as they rise through its hierarchical ranks in order to attain knowledge, virtue, and economic success. The dark side of education is constituted by the shame, discipline, and economic precarity which follow the failure to conform with its hierarchical regime.

However, alternative modes of study are always lurking in the shadow of education. Meyerhoff notes that these modes of study occur in the very same places, spaces, and institutions which we most closely identify with education—the university, for example.[^13] Indeed, cultural theorist Fred Moten and Marxist scholar Stefano Harney go as far as to argue that “the only possible relationship to the university today is a criminal one.”[^14] To adopt such a relationship is to disorient us from the campus as a space of education and to orient ourselves instead toward places that exist in the shadow of the ivory tower: the basement, the break room, the smoke deck, the illicit rooftop.

In those spaces, we find those whose study is illegible to the administration and those whose study is only legible as a threat. As Moten and Harney say, “ in the undercommons of the university they meet to elaborate their debt without credit, their debt without count… they meet those others who dwell in a different compulsion, in the same debt.”[^15] In the shadow of the university, other modes of study are possible. In the shadow of the university, our disorientation might produce other orientations; as Moten and Harney put it, we might orient ourselves toward “that undercommon sensuality, that radical occupied-elsewhere, *that utopic commonunderground of this dystopia,* the funked-up here and now of this anacentric particularity that we occupy and with which we are preoccupied.”[^16] In the undercomons of the University, we might begin to practice modes of study which value our well-being and facilitate human flourishing. As authors Emily and Amelia Nagoski say: “the cure for burnout is not self-care: it is all of us caring for each other.”[^17] Thus, disorientation might be the only cure for burnout: disorienting ourselves from higher education might be the catalyst for orienting ourselves toward each other.

## Problematizing the structural [com]position of “the university”

We have good reason to disorient ourselves from study as education—that is, from study as imagined by university administrators. We should be disoriented by the participation of ‘the university’ in global structures of violence. Or to be more precise, we should be disturbed by how Western institutions of higher education are enmeshed within larger networks of extraction, harm, and death which render their promises of humane progress hollow at best and pernicious at worst. Further, the university’s intimate proximity to these networks of violence engenders an gravitational force which reshapes the structure of the university in its image. 

As Moten and Harney note, “The slogan on the Left, then, ‘universities, not jails,’ marks a choice that may not be possible. In other words, perhaps more universities promote more jails. Perhaps it is necessary finally to see that the university produces incarceration as the product of its negligence.”[^18] Indeed, the university promotes more than jails. We might remember the intimate connections between between MIT, Stanford, and the techno-economies of surveillance capitalism; or between Harvard, Columbia, state-school area studies programs, quasi-public think tanks, and Cold War neo-imperialism; or, as we previously noted, between land-grant universities and settler-colonial genocide. Ethnic studies scholar Julia Chinyere Oparah terms this network of relationships the military-prison-industrial-complex and argues that the university participates in “mutually reinforcing” relationships with these institutions that produce structural violence.[^19] Let us expand Oparah’s multi-hyphenate-term to the unwieldy *colonial-military-prison-industrial-surveillance complex* in an effort to get at how these myriad domains take advantage of higher education in similar ways. Namely: these private and quasi-public institutions use the university as both a R&D lab to refine their techniques of extraction and violence at the same time as they use the university as a training facility to ‘educate’ (read: produce) amenable, qualified labor.

The colonial-military-prison-industrial-surveillance complex is not only interested in the knowledge of the university, but in the students—or rather, labor—that the university produces. Thus, the hierarchical disciplinary structures which define these private and quasi-public entities effect not just the position, but the *composition* of the not-so-ivory-tower. Critical education scholar Roderick Ferguson describes how the bureaucratic administration of the university grew during the 20th century not only in response to the growing entanglement with the multi-hyphenate complex, but in response to student refusal and rebellion.[^20] The institution of university administrative bureaucracy works to channel radical student demands inward—to reduce calls for structural change to a form which does not threaten the status quo. That is, the administrative composition of the university works to render political claims as personal grievance by rendering the student not as a political equal with valid claims over the structure of their community, but as a consumer-like individual whose discontent can be managed.[^21] The bureaucratization of student demands implicitly reveals the power of student action: our actions have power because we are no small part of the labor which maintains the university. We must remember that our voices matter, that our demands carry weight—regardless of the administration’s efforts to make us think otherwise.

Disorienting ourselves from the university as such means orienting ourselves toward each other. Disorienting ourselves from the university means refusing the individualization of education-as-study and committing ourselves to practices of study which build solidarity and connection in unexpected ways instead of pitting us in competition against our peers. Disorienting ourselves from the university means rebelling against management and articulating our political claims in the open, where all can see. Disorienting ourselves from the university means orienting ourselves toward an abolitionist dream of what studying-together could be: one in which we cannot possibly imagine the university as a node in the multi-hyphenate economies of violence and death. Oparah asks: “Can we imagine a university run by and for its constituents, including students, kitchen and garden staff, and tenure-track and adjunct faculty?”[^22] We can—no, we must. We must disorient ourselves so we can orient ourselves toward a better world.

## Being-otherwise in relation to higher education

How, then, do we disorient ourselves? What would it mean to be-otherwise in relation to higher education? Such disorientation would begin by refusing both the ‘Badger’ identity in particular and the administration of student life in general. Disorientation from higher education might entail a rejection of the label of ‘student’ altogether—a rejection of the idea that our participation in education defines our being. In doing so, we must aim to remain more-than-legible to the institutions of the university and lead rich, multiplicitous lives which can never be entirely managed by the administration. To start, we should take our lunch breaks outside of the unions; we should build networks of friends independent from the social prosthetics offered by the administration; we should protect the boundaries between our education and our life, between the classroom and the bedroom.

By disorienting ourselves from the institution of the university and education as a mode of study, we create the possibility to (re)orient ourselves toward each other and counter hegemonic, liberatory modes of study. This is an urgently necessary move: the university does not—and in fact, can not—prioritize our well being; in order to sustain ourselves, we must find (or: create) community which will. As we study otherwise—or, with wise others—we must learn with and from counter-hegemonic practices of study such as Black study and indigenous practice of study. Following these marginalized traditions, our practices of studying-otherwise must always-already exceed discourse and critique. As abolitionist scholars Abigail Boggs, Eli Meyerhoff, Nick Mitchell, and Zach Schwartz-Weinstein declare, “abstract oppositionality and critique, left to their own devices, may in fact unwittingly reproduce [structures of extraction and exploitation] by offering their practitioners the sense of moral supremacy and social exteriority necessary to imagine knowledge production as a form of change in itself.”[^23] Thought, discourse, and critique must be tethered to action and rendered accountable to others if we aim to realize our abolitionist ‘freedom dreams.’ Indeed, the abolitionist university is not a discursive fiction, but a network of relationships with “various potentials for transforming what and whom the university can be for.”[^24]

While we bide our time in the undercommons, in those spaces and places which escape the watchful eyes of soaring administrators, let us take heed of Moten & Harney’s observation that “[although] it cannot be denied that the university is a place of refuge, and it cannot be accepted that the university is a place of enlightenment.”[^25] With the knowledge of the university’s integral role in perpetuating economies of violence and death, we are forced to conclude that it is impossible to have an ethical education. Thus, we must come to terms with Moten & Harney’s imperative: “In the face of these conditions one can only sneak into the university and steal what one can.”[^26] Yet as we study for more than just credit, we might imagine other relationships: ones which emerge in the face of different conditions.

As we steal the bricks of ivory, we can begin to build different kinds of shelter in the tower’s shadow. In the shadow, we will build our own forum from which to speak: an arena to declare our demands of the university as political equals. We will build our capacity to refuse the demands of the university and withdraw our consent from its participation in networks of structural violence. We will build our communities to be ungovernable, and when they try to discipline us we will laugh and dance and sing. Our refusal to be managed, our rebellion against the multi-hyphenate complex, and our not-so-petty thievery is always about survival, but is more than just necessary: out of sight and out of [our] minds, we remain disoriented as we orient ourselves toward each other, toward other ways of study, other ways of being—to what is otherwise possible, and toward other possible worlds.


## References
[^1]: There seems to be no official location for UW–Madison's standard land acknowledgment statement, but it is available on many [department websites including the Department of Gender and Women's Studies.](https://gws.wisc.edu/about-us/land-acknowledgment/)

[^2]: Robert Lee and Tristan Ahtone, [“Land-grab universities,” *High Country News,*]( https://www.hcn.org/issues/52.4/indigenous-affairs-education-land-grab-universities) 30 March 2020. See also their [interactive map](https://www.landgrabu.org/universities).

[^3]: [“A Brief History of the Winnebago Tribe,” *HoChunk Renaissance: Official Language Program of the Winnebago Tribe.*](http://www.hochunklanguage.com/index.php/about/tribal-history/41-a-brief-history-of-the-winnebago-tribe)

[^4]: Aaron Bird Bear, [“Notes from the Ethnic Cleansing Zone,” *Forward? The Wisconsin Idea, Past & Present,*](https://youtu.be/02gAy37Aa9Q?t=2248), 29 November 2016, 37:28.

[^5]: Chelsea Vowel, [“Beyond territorial acknowledgments,” *âpihtawikosisân,*](https://apihtawikosisan.com/2016/09/beyond-territorial-acknowledgments/) 23 September 2016.

[^6]: See the last section, “Vision Statement.” [“About us,” SOAR: Student Orientation, Advising, and Registration.](https://soar.wisc.edu/about-us/)

[^7]: Sara Ahmed, Queer Phenomenology (Durham: Duke University Press, 2006), 157.

[^8]: Ahmed, 158-59.

[^9]: [“Job burnout: How to spot it and take action,” *Mayo Clinic,*](https://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/burnout/art-20046642) 20 November 2021.

[^10]: Eli Meyerhoff, *Beyond Education: Radical Studying for Another World* (Minneapolis: University of Minnesota Press, 2019), 13-15.

[^11]: Meyerhoff, 13.

[^12]: Meyerhoff, 15.

[^13]: Meyerhoff, 15.

[^14]: Fred Moten and Stefano Harney, *The Undercommons: Fugitive Planning and Black Study,* (Brooklyn: Autonomedia, 2013), 15.

[^15]: Moten and Harney, 68.

[^16]: Moten and Harney, 51. Emphasis mine.

[^17]: Emily Nagoski and Amelia Nagoski, [“Burnout,” *XOXO Festival,*](https://www.youtube.com/watch?v=BOaCn9nptN8) September 2019.

[^18]: Moten and Harney, 41.

[^19]: J.C. Oparah, “Challenging complicity: The neoliberal university and the prison-industrial complex,” in *The Imperial University: Academic Repression and Scholarly Dissent,* ed. Piya Chatterjee and Sunaina Maira (Minneapolis: Minnesota University Press: 2014), 110.

[^20]: Roderick Ferguson, *We Demand: The University and Student Protests.* (Oakland: University of California Press, 2017).

[^21]: Ferguson, 25-26, 68-69.

[^22]: Oparah, 117.

[^23]: Abigail Boggs, Eli Meyerhoff, Nick Mitchell, and Zach Schwartz-Weinstein, [“Abolitionist University Studies: an Invitation,”](https://abolition.university/invitation/) 2019, 28.

[^24]: Boggs, Meyerhoff, Mitchell, and Schwartz-Weinstein, 28.

[^25]: Moten and Harney, 24.

[^26]: Moten and Harney, 24.
